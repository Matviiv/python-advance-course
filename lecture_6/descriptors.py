class IntDescriptor:
    def __get__(self, instance, owner):
        return self._value

    def __set__(self, instance, value):
        assert isinstance(value, int), 'should be integer'
        self._value = value

    def __delete__(self, instance):
        del self._value


class StringDescriptor:
    def __get__(self, instance, owner):
        return self._value

    def __set__(self, instance, value):
        assert isinstance(value, str) and len(value) > 2, 'should be at least 3 characters long'
        self._value = value

    def __delete__(self, instance):
        del self._value
