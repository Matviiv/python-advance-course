from descriptors import IntDescriptor, StringDescriptor


class RadaType(type):
    def __new__(cls, clsname, bases, dct):
        if 'int_types' in dct:
            for value in dct['int_types']:
                dct[value] = IntDescriptor()

        if 'str_types' in dct:
            for value in dct['str_types']:
                dct[value] = StringDescriptor()

        return type.__new__(cls, clsname, bases, dct)

