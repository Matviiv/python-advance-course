# Зробити генератор - аналог ф-ї enumarate , має yield -иди  індекс і значення.
import random


def en_custom(collection):
    index = 0
    for x in collection:
        yield index, x
        index += 1


if __name__ == '__main__':
    li = range(15, random.randint(15, 20))

    for index, elem in en_custom(li):
        print(f'element: {elem}', f'index: {index}')

    print('-----')

    for index, elem in enumerate(li):
        print(f'element: {elem}', f'index: {index}')

    print(list(li))