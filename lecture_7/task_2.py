# 2) Зробити coroutine.   Функція seen_words.  Суть полягає у тому,
#  що треба відправити ( send) слово у couroutine, відповідь має прийти
#  ( True або False) True якщо таке слово вже було відправлено. False,
#  якщо такого слова не було.
from functools import wraps


def coroutine(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        gen = func(*args, **kwargs)
        next(gen)
        return gen
    return wrapper


@coroutine
def seen_words():
    cached_words = set()
    result = False
    while True:
        word = yield result
        if word in cached_words:
            result = True
        else:
            result = False
            cached_words.add(word)
        print(result)


if __name__ == '__main__':
    gen = seen_words()
    gen.send('RAID 0')