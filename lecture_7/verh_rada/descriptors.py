from weakref import WeakKeyDictionary


class CounterDesc:
    counter = 0

    def __init__(self):
        type(self).counter += 1

    def __del__(self):
        type(self).counter -= 1


class StringDescriptor(CounterDesc):
    def __init__(self):
        super(StringDescriptor, self).__init__()
        self.memory = WeakKeyDictionary()

    def __get__(self, instance, owner):
        return self.memory[instance]

    def __set__(self, instance, value):
        assert len(value) > 2, 'name should be at least 3 characters length.'
        self.memory[instance] = value

    def __delete__(self, instance):
        del self.memory[instance]
