from descriptors import StringDescriptor


class RadaType(type):
    def __new__(cls, clsname, bases, dct):
        if 'str_types' in dct:
            for value in dct['str_types']:
                dct[value] = StringDescriptor()

                print(f'Descriptor has been created count: {dct[value].counter}')

        return type.__new__(cls, clsname, bases, dct)

