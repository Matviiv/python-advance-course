# 3) Зробити coroutine.
# Має бути ф-я генератор ( gen_numbers), що генерує числа від 1 до 100. Ця ф-я має відправляти дані
# у even_filter ( генератор, що приймає дані) якщо число парне то відправити його у наступний фільтр.
# filter_40_70 ( coroutine ), що приймає число і якщо воно в діапазоні від 40 до 70
# відправляє його у file_coroutine . file_coroutine  - приймає числа і записує іх у файл
# final_numbers.txt, кожна число із нового рядка.
# ( таким чином має бути 1 генератор + 2 фільтри + 1 consumer для запису у файл).
# Використати декоратор coroutine для виклику 1 раз next.
from functools import wraps
import os
import time

# get_numbers -> even_filter -> filter_40_70 -> file_coroutine

db_file = './files/final_numbers.txt'


def coroutine(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        gen = func(*args, **kwargs)
        next(gen)
        return gen
    return wrapper


def get_numbers():
    for x in range(1, 101):
        yield x
        even_filter().send(x)


@coroutine
def even_filter():
    while True:
        inp = yield
        if inp % 2 == 0:
            filter_40_70().send(inp)


@coroutine
def filter_40_70():
    while True:
        res = yield
        if res in range(40, 71):
            file_coroutine().send(res)


@coroutine
def file_coroutine():
    while True:
        number = yield
        with open(db_file, 'a') as f:
            f.write(str(number) + '\n')


if __name__ == '__main__':
    print('triggering generators chain . . .')
    list(get_numbers())

    choice = input('Remove db file: yes/no: ')
    if choice == 'yes':
        os.remove(db_file)



